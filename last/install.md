>Для работы ClerkCloud обязательно нужен MongoDB. Вы можете установить его по [этой ссылке](https://docs.mongodb.com/manual/installation/)

В этой секции документации находятся инструкции по настройке и запуску ClerkCloud

Для начала нам необходимо скачать ClerkCloud. Выберите версию, которая подходит для вас.

ClerkCloud version 0.0.1:
* <a href="/src/windows32/Clerk.exe">Windows 32</a><br>
* <a href="/src/windows64/Clerk.exe">Windows 64</a><br>
* <a href="/src/linux32/Clerk">linux 32</a><br>
* <a href="/src/linux64/Clerk">linux 64</a><br>
* <a href="/src/macos32/Clerk">MacOS 32</a><br>
* <a href="/src/macos64/Clerk">MacOS 64</a><br>


Следующий этап: [настройка](configurate.md)