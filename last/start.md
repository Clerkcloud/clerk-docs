Перед запуском сервера введём команду

```
Clerk server --help
```
*Вместо Clerk введите команду для запуска конкрето вашего клерка*

Нам выведет 

```
Interface for control Clerk server in background mode.
Use: server <command>
The following commands for managing the server state:
        start   - start server in background mode.
        stop    - stop server in background mode.
        restart - restart server in background mode.
        console - start server at console.

Usage:
  Clerk server [flags]

Flags:
  -h, --help   help for server
```

Как мы видим, для работы с сервером доступны 4 команды:

* server start   - запуск сервера в фоновом режиме.
* server stop    - остановка сервера из фонового режима.
* server restart - перезапуск сервера из фонового режима.
* server console - запуск сервера в самой консоли.

Запустим сервер в фоновом режиме:

```
server start
```
Если не вывелось сообщений об ошибках, значит завелась шарманка.

Теперь создадим пустой файл и вставим туда наш код:

```
Hero model{
fields:
    hello string!
    age int
configs:
	database.driver: "mongo"
	database.address: "localhost"
}

//hero POST{
    hello string!
    age int{
        defValue: len(hello)
    }
}(
    hero.create({
        "hello": hello,
        "age":age
    })
)

//sum/{int1}/{int2} GET{
    int1 int!
    int2 int!
}(
    int1 + int2
)

//mult/{int1}/{int2} GET{
    int1 int!
    int2 int!
}(
    int1 * int2
)
```

Сохраним файл.

Теперь посмотрим как загрузить файл на сервер:

```
$Clerk add --help

Add new project with code to server.
        Use: add <project name> <path>
        <project name>  - unique project name for server
        <path>          - path to the repository with the project code.

Usage:
  Clerk add [flags]

Flags:
  -h, --help   help for add
```

Как мы видим, после add, мы должны указать имя проекта, и путь до файла проекта.

Введём:
```
Clerk add HeroProject youFileName.txt
```
Если не было сообщений об ошибке, значит всё прошло успешно :)
>Эту безмолвность сервера тоже постораемся скоро исправить

Теперь, давайте посмотрим на нашу документацию по серверу:
перейдём по адресу:
```
localhost:3000/docs
```
порт указывайте тот, который вы указали в config.yaml

Нам должен открыться спискок проектов, в данном случае только один проект. Переходим по ссылке и попадаем на список роутов с методами и параметрами. Этот api можно вызывать следующим образом:

```
localhost:3000/heroproject/<path>
```
где path - это путь из api.
Сделано именно таким образом для того, чтобы иметь возможность через nginx сделать прокси через субдомены.
Т.е. Можно будет обращаться ко всем проектам таким образом:
```
Project.youDomen.com/apiPath
```