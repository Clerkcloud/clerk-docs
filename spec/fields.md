Поля сущности описывают доустпную структуру данных, которую имеет сущность. Каждое поле имеет тип. Существует поля типа ``string``, ``int``, ``float``,  ``object`` и ``hash``. Поле может принимать значение соответсвтующее своему типу или значени ``nil``. Если после типа поля стоит знак ``!`` _(пример: ``string!``)_, то поле не может принимать значение ``nil``. В ином случае, все поля по умолчанию имеют значение ``nil``. Так же у всех полей есть состояние ``unchanged``, это когда поле равно ``nil``, которое никем не задавалось. Если заного присвоить полю ``nil``, ``unchanged`` станет равно false. ``unchanged`` равно true до тех пор, пока ему не задали значение. Значение по-умолчанию нарушает ``unchanged``. Так же очевидно, что у не ``nil`` полей ``unchanged`` всегда равно ``false``.

Так же каждое поле имеет свои конфиги.
***
## Общие конфиги для полей

### access.reade 
Тип ``bool``

Управляет доступом на чтение данного поля сущности. Значение, по умолчанию, наследуется от конфига access.read 
сущности.

### access.update
Тип ``bool``

По умолчанию ``false`` 

Управляет доступом на изменение данного поля сущности. Значение, по умолчанию, наследуется от конфига access.update сущности.

### uniq
Тип ``bool``

По умолчанию ``false``

Указывает на уникальность поля среди всех объектов сущности.


### uniq
Тип ``bool``

По умолчанию ``false``

Указывает на уникальность поля среди всех объектов сущности.

### Символ ! после типа. "type!" 
По умолчанию ``false``

Указывает на обязательность поля. Поле не может быть ``nil``.

---
## Поле string
Поле для хранения строковых значений неограниченной длины в формате UTF.
Доступные конфиги

### defValue
Тип ``string``

Устанавливает значение по умолчанию, которое можно изменить. По умолчанию ``nil``.

### maxLen
Тип ``int``

Устанавливает максимальную длину строки

### minLen
Тип ``int``

Устанавливает минимальную длину строки

### value
Тип ``string``

Устанавливает значение, которое нельзя никак изменить, если оно не равно ``nil``. По умолчанию ``nil``.

---
## Поле int
Поле для хранения целочисленных значений от -9223372036854775808 до 9223372036854775807.
Доступные конфиги:

### defVal
Тип ``int``

Устанавливает значение по умолчанию, которое можно изменить. По умолчанию ``nill``

### maxVal
Тип ``int``

Устанавливает максимальное значение,которое можно установить.

### minLen
Тип ``int``

Устанавливает минимальное значение, которое можно установить.

### value
Тип ``int``

Устанавливает значение, которое нельзя никак изменить, если оно не равно ``nil``. По умолчанию ``nil``.

---
## Поле float
Поле для хранения чисел с плавающей точкой.
Доступные конфиги:

### defVal
Тип ``float``

Устанавливает значение по умолчанию, которое можно изменить. По умолчанию ``nill``

### maxVal
Тип ``float``

Устанавливает максимальное значение,которое можно установить.

### minLen
Тип ``float``

Устанавливает минимальное значение, которое можно установить.

### value
Тип ``float``

Устанавливает значение, которое нельзя никак изменить, если оно не равно ``nil``. По умолчанию ``nil``.

### maxVal
Тип ``float``

Устанавливает максимальное значение,которое можно установить.

### minLen
Тип ``float``

Устанавливает минимальное значение, которое можно установить.

### value
Тип ``float``

Устанавливает значение, которое нельзя никак изменить, если оно не равно ``nil``. По умолчанию ``nil``.

---
## Поле типа Object
Поле, которое указывает на объекты сущностей. К примеру:

```
model Message{
    text string
    to   User
}
```
В данном примере поле <i>to</i> это поле типа ``object`` указывающая на объект сущности <i>User</i>. Значение этого поля будет равно GUID'у объекта User, на которую он будет указывать.

---
## Поле hash
Поле хранит хеш полученного значения