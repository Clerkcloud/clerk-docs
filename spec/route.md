Роуты используются для добавления новых эндпоинтов в вашем api. ``route`` состоит из пути, метода, принимаемых аргументов и действий. Пример роута:

```
/hero/new POST{
    name string!
    age int
}(
    Hero.create(.name = name, .age = age)
)
```

Где <i>/hero/new</i> это путь, _POST_ - http метод, _name_ и _age_ - доступные аргументы, а выражение _Hero.perform_ это действие.

## Доступные методы для роутинга:
* GET
* POST
* PATCH
* DELETE